/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
 
 
class Solution {
    public TreeNode sortedArrayToBST(int[] nums) {
       /*   if (nums.length == 0 || nums ==null)
            return null; 
            */
        TreeNode result = helper (nums, 0, nums.length-1);
        return result;
    }
    
    private TreeNode helper (int [] arr, int start, int end){
        if(start>end)
            return null;
        
        int middle = (start+end)/2;
        
        TreeNode root = new TreeNode(arr[middle]);
        root.left = helper(arr, start, middle-1);
        root.right = helper(arr, middle+1, end);
        
        return root;
    }
}